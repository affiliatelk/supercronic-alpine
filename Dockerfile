# https://github.com/billyteves/alpine-golang-glide
FROM billyteves/alpine-golang-glide:1.2.0 as builder
WORKDIR /go/src/github.com/aptible/supercronic/

# Clone repository and build supercronic
RUN git clone https://github.com/aptible/supercronic.git . && \
    glide install && \
    make build

# Build final image straight from alpine
FROM alpine:latest
WORKDIR /root/
COPY --from=builder /go/src/github.com/aptible/supercronic/ .
ENTRYPOINT ["./supercronic"]